# Bmp Proxy

This repo builds a very basic proxy server (based out of browser mob proxy)

## Pre-requisites

* Maven
* JDK17
* Docker (builds generate a docker image as well)

## Building the code

```shell
mvn clean package
```

## Running the server

```shell
java -Dproxy.port=4445 -Dserver.port=6666 -jar target/bmp_proxy-1.0-SNAPSHOT.jar
```

This command brings up an app server listening on `6666` and a proxy server listening on `4445`

Following commands are supported by the app server, to alter the proxy server behaviour.

```http request
### Introduce delays of 10 seconds

GET http://localhost:6666/api/v1/delay?delay=10&unit=seconds HTTP/1.1

### Disable delays (Call without any query params)
GET http://localhost:6666/api/v1/delay HTTP/1.1

### Block Heart beats

GET http://localhost:6666/api/v1/heartbeat?block=true HTTP/1.1

### Enable Heart beats

GET http://localhost:6666/api/v1/heartbeat HTTP/1.1

### Fudge Job status to indicate failures

GET http://localhost:6666/api/v1/job-status?mark-failed=true HTTP/1.1

### Reset Job status fudging

GET http://localhost:6666/api/v1/job-status HTTP/1.1

### Block Job status

GET http://localhost:6666/api/v1/job-status-block?block=true HTTP/1.1

### Enable Job status

GET http://localhost:6666/api/v1/job-status-block HTTP/1.1


```