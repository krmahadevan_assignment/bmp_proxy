package com.rationaleemotions;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ProxyServer.start();
        AppServer.start();
        SharedState.instance.dumpCurrentState();
    }
}
