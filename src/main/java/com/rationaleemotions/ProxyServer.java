package com.rationaleemotions;

import com.rationaleemotions.filters.DelayResponseFilter;
import com.rationaleemotions.filters.HeartBeatBlockingFilter;
import com.rationaleemotions.filters.JobStatusFudgingFilter;
import com.rationaleemotions.filters.LoggingFilter;
import lombok.extern.slf4j.Slf4j;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;

import java.util.List;

@Slf4j
public final class ProxyServer {

    public static void start() {
        int port = Integer.parseInt(System.getProperty("proxy.port", "7070"));
        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.start(port);
        List.of(new JobStatusFudgingFilter(), new DelayResponseFilter(), new HeartBeatBlockingFilter(),
                        new LoggingFilter())
                .forEach(proxy::addRequestFilter);
        log.info("Proxy Settings to use [localhost:" + proxy.getPort() + "]");
        Runtime.getRuntime().addShutdownHook(new Thread(proxy::stop));
    }
}
