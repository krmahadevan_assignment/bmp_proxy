package com.rationaleemotions.filters;

import io.netty.handler.codec.DecoderResult;
import io.netty.handler.codec.http.*;

public class DummyResponse implements HttpResponse {

    private final HttpRequest request;

    public DummyResponse(HttpRequest request) {
        this.request = request;
    }

    @Override
    public HttpResponseStatus getStatus() {
        return HttpResponseStatus.OK;
    }

    @Override
    public HttpResponse setStatus(HttpResponseStatus status) {
        return null;
    }

    @Override
    public HttpVersion getProtocolVersion() {
        return HttpVersion.HTTP_1_1;
    }

    @Override
    public HttpResponse setProtocolVersion(HttpVersion version) {
        return null;
    }

    @Override
    public HttpHeaders headers() {
        return request.headers();
    }

    @Override
    public DecoderResult getDecoderResult() {
        return request.getDecoderResult();
    }

    @Override
    public void setDecoderResult(DecoderResult result) {

    }
}
