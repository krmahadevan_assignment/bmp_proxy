package com.rationaleemotions.filters;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rationaleemotions.SharedState;
import io.netty.handler.codec.http.*;
import lombok.extern.slf4j.Slf4j;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

@Slf4j
public class JobStatusFudgingFilter implements RequestFilter {

    private final Gson gson = new Gson();

    @Override
    public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents, HttpMessageInfo messageInfo) {
        if (!request.getMethod().equals(HttpMethod.POST)) {
            return null;
        }
        if (!request.getUri().endsWith("/job-status")) {
            return null;
        }
        if (SharedState.instance.isSimulateJobFailures()) {
            String current = contents.getTextContents();
            JsonObject jsonObject = gson.fromJson(current, JsonObject.class);
            if (jsonObject.has("status")) {
                log.info("Simulating Job failures");
                jsonObject.addProperty("status", "Failed");
                contents.setTextContents(jsonObject.toString());
            }
            return null;
        }
        if (SharedState.instance.isBlockJobStatus()) {
            log.info("Blocking Job status from reported upstream.");
            return new DummyResponse(request);
        }
        return null;
    }
}
