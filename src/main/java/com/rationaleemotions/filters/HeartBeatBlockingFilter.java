package com.rationaleemotions.filters;

import com.rationaleemotions.SharedState;
import io.netty.handler.codec.http.*;
import lombok.extern.slf4j.Slf4j;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

@Slf4j
public class HeartBeatBlockingFilter implements RequestFilter {
    @Override
    public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents, HttpMessageInfo messageInfo) {
        if (!SharedState.instance.isBlockHeartBeats()) {
            return null;
        }
        if (!request.getUri().endsWith("/heartbeat")) {
            return null;
        }
        log.info("Blocking heartbeat from being sent to downstream server");
        return new DummyResponse(request);
    }
}
