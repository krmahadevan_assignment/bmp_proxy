package com.rationaleemotions.filters;

import com.rationaleemotions.SharedState;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

import java.util.concurrent.TimeUnit;

@Slf4j
public class DelayResponseFilter implements RequestFilter {
    @Override
    public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents, HttpMessageInfo messageInfo) {
        if (!SharedState.instance.isIntroduceDelays()) {
            return null;
        }
        try {
            long millis = SharedState.instance.getDuration().toMillis();
            log.info("Simulating delays by sleeping for {} ms", millis);
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return null;
    }
}
