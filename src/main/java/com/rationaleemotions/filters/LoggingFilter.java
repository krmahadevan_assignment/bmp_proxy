package com.rationaleemotions.filters;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

import java.util.List;
import java.util.Map;

@Slf4j
public class LoggingFilter implements RequestFilter {

    private static final List<String> IMPORTANT_HEADERS = List.of("content-type", "user-agent",
            "content-length", "host");
    @Override
    public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents, HttpMessageInfo messageInfo) {
        log.debug("Received {} from {} with headers {}", request.getMethod(), request.getUri(), headers(request));
        return null;
    }

    private List<Map.Entry<String, String>> headers(HttpRequest request) {
        return request.headers().entries().stream()
                .filter(it -> IMPORTANT_HEADERS.contains(it.getKey().toLowerCase()))
                .toList();


    }
}
