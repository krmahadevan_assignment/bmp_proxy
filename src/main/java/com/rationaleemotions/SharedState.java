package com.rationaleemotions;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.Map;

@Slf4j
@Getter
public enum SharedState {
    instance;

    private volatile boolean introduceDelays = false;
    private volatile boolean blockHeartBeats = false;
    private volatile boolean simulateJobFailures = false;
    private volatile boolean blockJobStatus = false;
    private Duration duration = Duration.ofMillis(0L);


    public void setBlockJobStatus(boolean blockJobStatus) {
        this.blockJobStatus = blockJobStatus;
    }

    public void setSimulateJobFailures(boolean simulateJobFailures) {
        this.simulateJobFailures = simulateJobFailures;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public void setIntroduceDelays(boolean introduceDelays) {
        this.introduceDelays = introduceDelays;
    }

    public void setBlockHeartBeats(boolean blockHeartBeats) {
        this.blockHeartBeats = blockHeartBeats;
    }

    public void dumpCurrentState() {
        Map<String, Object> data = Map.of(
                "blockHeartBeats", blockHeartBeats,
                "introduceDelays", introduceDelays,
                "blockJobStatus", blockJobStatus,
                "simulateJobFailures", simulateJobFailures,
                "delayDuration", duration
        );
        log.info("Current state: {}", data);
    }
}
