package com.rationaleemotions;

import com.rationaleemotions.handlers.DelayHandler;
import com.rationaleemotions.handlers.HeartBeatBlockHandler;
import com.rationaleemotions.handlers.JobStatusBlockHandler;
import com.rationaleemotions.handlers.JobStatusFailureSimulatingHandler;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AppServer {

    public static void start() {
        int port = Integer.parseInt(System.getProperty("server.port", "6666"));
        log.info("Running the Proxy Server on port: " + port);
        RoutingHandler routingHandler = Handlers.routing();
        routingHandler.get("/api/v1/delay", new DelayHandler());
        routingHandler.get("/api/v1/heartbeat", new HeartBeatBlockHandler());
        routingHandler.get("/api/v1/job-status", new JobStatusFailureSimulatingHandler());
        routingHandler.get("/api/v1/job-block", new JobStatusBlockHandler());
        Undertow server = Undertow.builder()
                .addHttpListener(port, "localhost")
                .setHandler(routingHandler)
                .build();
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(server::stop));
    }
}
