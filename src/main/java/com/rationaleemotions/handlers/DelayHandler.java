package com.rationaleemotions.handlers;

import com.rationaleemotions.SharedState;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;

public class DelayHandler implements HttpHandler {
    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) {
        Map<String, Deque<String>> queryParams = httpServerExchange.getQueryParameters();
        int delay = Optional.ofNullable(queryParams.get("delay"))
                .map(Deque::element)
                .map(Integer::parseInt)
                .orElse(0);
        if (delay <= 0) {
            SharedState.instance.setIntroduceDelays(false);
        } else {
            DurationUnit durationUnit = Optional.ofNullable(queryParams.get("unit"))
                    .map(Deque::element)
                    .map(String::trim)
                    .map(DurationUnit::fromString)
                    .orElse(DurationUnit.SECONDS);
            SharedState.instance.setIntroduceDelays(true);
            SharedState.instance.setDuration(durationUnit.apply(delay));
        }
        SharedState.instance.dumpCurrentState();
    }
}
