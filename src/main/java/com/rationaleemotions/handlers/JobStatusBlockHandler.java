package com.rationaleemotions.handlers;

import com.rationaleemotions.SharedState;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;

public class JobStatusBlockHandler extends AbstractBlockingHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        boolean state = state(exchange);
        SharedState.instance.setBlockJobStatus(state);
        SharedState.instance.setSimulateJobFailures(false);
        SharedState.instance.dumpCurrentState();
    }
}
