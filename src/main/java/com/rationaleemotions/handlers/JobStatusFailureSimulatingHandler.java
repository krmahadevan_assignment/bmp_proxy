package com.rationaleemotions.handlers;

import com.rationaleemotions.SharedState;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;

public class JobStatusFailureSimulatingHandler implements HttpHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        Map<String, Deque<String>> queryParams = exchange.getQueryParameters();
        Boolean state = Optional.ofNullable(queryParams.get("mark-failed"))
                .map(Deque::element)
                .map(Boolean::parseBoolean)
                .orElse(false);
        SharedState.instance.setSimulateJobFailures(state);
        SharedState.instance.setBlockJobStatus(false);
        SharedState.instance.dumpCurrentState();
    }
}
