package com.rationaleemotions.handlers;

import com.rationaleemotions.SharedState;
import io.undertow.server.HttpServerExchange;

public class HeartBeatBlockHandler extends AbstractBlockingHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        boolean state = state(exchange);
        SharedState.instance.setBlockHeartBeats(state);
        SharedState.instance.dumpCurrentState();
    }
}
