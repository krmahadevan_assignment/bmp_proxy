package com.rationaleemotions.handlers;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractBlockingHandler implements HttpHandler {

    public boolean state(HttpServerExchange exchange) {
        Map<String, Deque<String>> queryParams = exchange.getQueryParameters();
        return Optional.ofNullable(queryParams.get("block"))
                .map(Deque::element)
                .map(Boolean::parseBoolean)
                .orElse(false);
    }
}
