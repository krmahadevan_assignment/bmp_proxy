package com.rationaleemotions.handlers;

import java.time.Duration;
import java.util.Optional;
import java.util.function.Function;

public enum DurationUnit implements Function<Integer, Duration> {

    MILLIS {
        @Override
        public Duration apply(Integer integer) {
            return Duration.ofMillis(integer);
        }
    }, SECONDS {
        @Override
        public Duration apply(Integer integer) {
            return Duration.ofSeconds(integer);
        }
    };


    public static DurationUnit fromString(String unit) {
        return Optional.ofNullable(unit)
                .map(it -> DurationUnit.valueOf(it.toUpperCase()))
                .orElse(MILLIS);
    }

}
